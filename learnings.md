# Day 1 - 1st October,2021 Friday

- revised javascript functions
- used chrome snippet feature in dev tool

# Day 2 - 2nd October,2021 Saturday

- Written pseudo code on functions
- Practiced and read about function chaining,currying, behaviour of this with methods and functions
- Tried password less login on server
- adding ssh keys
- worked on documentation

# Day 3 - 3rd October,2021 Sunday

- Revised Everything what i learned in last 10 days
    - linux
    - git
    - javascript basics
- worked on documentation

# Day 4 - 4th October,2021 Monday

- Written pseudo code of Array methods
    - push
    - pop
    - includes
    - shift
    - unshift
    - concat
    - map
    - filter
    - forEach
    - reduce
    - indexOf
- Read and tried different behaviour of this according to different context
- Practiced and read about promises, async await

# Day 5 - 5th October,2021 Tuesday

- Installed Node (learnt about npm, nvm)
- Read about semver
- read and practiced object oriented concepts
- new terms learnt - tight coupling,variable collision, variable shadowing, scope chain
- variables declared with var outside functions are attached to global object, variables declared with let and const outside function are not attached to global object
- window object is not javascript specification,it's part of browser

# Day 6 - 6th October,2021 Wednesday
